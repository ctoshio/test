/*
EJERCICIOS 
1) Pedir al usuario que ingrese su nombre. Guardar
ese nombre en una variable y utilizarlo para saludar al 
usuario (por ejemplo: "Hola Alexis").
*/

// const namea = prompt("What is your name?: ");

// alert("Hello" +' '+ namea);

/*2) Pedirle a un usuario que ingrese dos numeros
y devolverle la suma de dichos numeros
*/
// const numOne = prompt("Enter the first number: ")
// const numTwo = prompt("Enter the second number: ")

// alert(`the sum of ${numOne} and ${numTwo} are ${+numOne + +numTwo}`);


/*3) Pedirle a un usuario que ingrese dos numeros
y devolverle el doble de la suma de dichos numeros
*/
// const numOne = prompt("Enter the first number: ")
// const numTwo = prompt("Enter the second number: ")

// alert(`the sum of double of  ${numOne} and ${numTwo} are ${2*(+numOne + +numTwo)}`);



/*
4) Pedirle al usuario que ingrese el ancho y el alto 
de una habitacion y calcular la superficie.
*/
// const numOne = prompt("Enter the width: ")
// const numTwo = prompt("Enter the height: ")

// alert(`the area is ${+numOne * +numTwo}`);


/*
5) Pedirle al usuario que ingrese su nombre y devolverle
el nombre todo en minusculas
*/
// const numOne = prompt("Please enter your name: ")
// alert(`Your name ${numOne.toLowerCase()}`)
/*
6) Pedirle al usuario que ingrese su apellido y devolverle
el apellido todo en mayuscula.
*/


/*
7) Pedirle al usuario el nombre, el apellido y la 
edad. Mostrar el siguiente mensaje:
Nombre: Juan
Apellido: Perez
Edad: 20
*/



/*

*/
// let trian = '#';
// for(let i=0; i <= 5; i++){
//     trian = trian + '#'
//     console.log(trian)
// }

/*1) REGISTRO.
  Vamos a construir un programa que permita al usuario registrarse en nuestra aplicacion. Para ello, en primer lugar vamos a
  mostrar un mensaje que le de la bienvenida al usuario y le pregunte si desea registrarse (PISTA: recuerden que habia un metodo del 
  navegador que permitia hacer este tipo de consultas al usuario, devolviendo true o false segun el caso).
  En caso de que el usuario acepte registrarse, le vamos a pedir que ingrese su nombre de usuario en primer lugar. El nombre de usuario
  tiene que tener por lo menos 3 caracteres y no va a ser case sensitive, por lo que lo vamos a almacenar en minusculas, independientemente
  de como lo ingrese el usuario. Agregar la validacion necesaria para que en caso de que el usuario no cumpla con las concidiones, 
  se muestre un mensaje de error.
  Si el nombre de usuario es ingresado correctamente, le vamos a pedir al usuario que ingrese una contrasena, la que tiene que tener
  por lo menos 6 caracteres. La contrasena tiene que ser case sensitive, por lo que se respetan las mayusculas y minusculas segun lo ingrese
  el usuario.
  En caso de que la contrasena no cumpla con el requisito anterior, se debe mostrar un mensaje de error. De lo contrario, se debe mostrar un
  mensaje de exito informando al
   usuario que se completo el registro. (OTRA PISTA: Para ver como validar la longitud de un string, pueden ver el siguiente
    enlace: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/length )
*/

// class Humano {
//   constructor(name, age){
//     this.name = name;
//     this.age = age;
//   }

//   speak() {
//       console.log(`${this.name}, se llama el humano`);
//   }
// }

// class Estudiante extends Humano {
//   constructor(name, age, codigo){
//     super(name, age);
//     this.codigo = codigo;
//   }

//   speakE(){
//       console.log(`${this.codigo}, mi codigo`);
//   }
// }

// caca = new Estudiante('suy', 22, 1615224303);

// const keyst = Object.keys(caca);
// console.log(keyst);


// const payWithCredit = (price, isConected) =>
//     new Promise ((resolve, reject)=> {
//       const limit = 10000;
//       console.log("Processing payment ...");
//       if (!isConected) return;
//       setTimeout(() => {
//         if (price <= limit) resolve("Aproved Purchase");
//         else reject("Fail purchase");
//       }, 1000);
//     });

// console.log(payWithCredit(100000,true));
//then and catch
// console.log(
//   payWithCredit(1000000, false).then((respuesta) => {
//     console.log(respuesta);
//   }).catch((error) => console.log(error))
// );
// console.log("other task")
// console.log("other task")

// async await catch
// async function processPay(){
//   try {
//     const resp = await payWithCredit(100000, true);
//     console.log(resp);
    
//   } catch (error) {
//     console.log(error);
//   }
// }

// processPay();


const getDataFromApi = async () => {
  try {
    const response = await fetch(
      "https://jsonplaceholder.typicode.com/todos/1"
      );
    console.log(await response.json());
  } catch (error) {
    console.log("error=>", error);
  }
};

getDataFromApi();

